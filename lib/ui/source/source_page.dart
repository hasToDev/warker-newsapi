import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:warker_newsapi/core/core.dart';
import 'source_controller.dart';
import '../article/article_page.dart';

class SourcePage extends StatelessWidget {
  static const String id = '/source';

  SourcePage({
    Key? key,
  }) : super(key: key);

  final SourceController c = Get.find<SourceController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Get.arguments,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: GoogleFonts.lexend(
            textStyle: Theme.of(context).textTheme.titleLarge,
            color: Colors.white,
            letterSpacing: 1.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.purple,
        leadingWidth: 56.0,
        leading: IconButton(
          splashColor: Colors.purple[400],
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () async {
            await Future.delayed(const Duration(milliseconds: 125));
            Get.back();
          },
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
        ),
      ),
      body: GetX<SourceController>(
        init: c,
        initState: (_) {
          c.clearListFromPreviousResult();
          c.getNewsSource('${Get.arguments}'.toLowerCase());
        },
        builder: (value) {
          // loading
          if (value.isLoading.isTrue) {
            return const Center(
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: CircularProgressIndicator(
                  color: Colors.purple,
                  strokeWidth: 3.0,
                ),
              ),
            );
          }

          // empty list
          if (value.sourceList.isEmpty && value.isError.isFalse) {
            return Center(
              child: Text(
                'No Data',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: Colors.purple,
                      fontWeight: FontWeight.w600,
                    ),
              ),
            );
          }

          // try button after error happened
          if (value.sourceList.isEmpty && value.isError.isTrue) {
            return Center(
              child: ElevatedButton(
                onPressed: () {
                  c.clearListFromPreviousResult();
                  c.getNewsSource('${Get.arguments}'.toLowerCase());
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    'try again',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.8,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ),
              ),
            );
          }

          // content
          return ListView.builder(
            padding: const EdgeInsets.only(top: 6.0, bottom: 20.0),
            shrinkWrap: true,
            controller: c.scrollController,
            itemCount: c.sourceList.length,
            itemBuilder: (context, index) {
              return SourceCard(data: c.sourceList[index]);
            },
          );
        },
      ),
    );
  }
}

/// SOURCE CARD --------------------------------------------
class SourceCard extends StatelessWidget {
  final CategoryData data;

  const SourceCard({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(ArticlePage.id, arguments: {'id': data.id, 'name': data.name});
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4.0),
        child: Card(
          elevation: 1.0,
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            child: Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.name ?? 'No Name',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      const SizedBox(height: 14.0),
                      Text(
                        data.description ?? 'No Description',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                              color: Colors.grey[900],
                              fontWeight: FontWeight.normal,
                            ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
