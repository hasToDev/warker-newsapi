import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import '../../core/core.dart';

class SourceProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.addAuthenticator((Request request) async {
      request.headers['X-Api-Key'] = "c0c7ce11c9d546d88bfbdcf1574d23d6";
      return request;
    });

    httpClient.timeout = const Duration(seconds: 30);
    httpClient.maxAuthRetries = 3;
  }

  // News Category
  Future<NewsCategory> getNewsSource(String category) async {
    final response = await get(
      "https://newsapi.org/v2/top-headlines/sources?category=$category",
    );
    if (response.status.hasError) {
      if (response.status.connectionError) {
        throw Exception('${response.statusText}');
      } else {
        throw ServerException(
          message: response.body ?? response.statusText,
          code: response.statusCode ?? 0,
        );
      }
    } else {
      return NewsCategory.fromJson(response.body);
    }
  }
}
