import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'source_provider.dart';
import '../../core/core.dart';

class SourceController extends GetxController {
  late ScrollController scrollController;

  @override
  void onInit() {
    scrollController = ScrollController();
    super.onInit();
  }

  RxInt count = 0.obs;
  RxList<CategoryData> sourceList = <CategoryData>[].obs;
  RxBool isLoading = false.obs;
  RxBool isError = false.obs;

  void callSnackBar(String title, String description) {
    Get.snackbar(
      title,
      description,
      duration: const Duration(seconds: 4),
      snackPosition: SnackPosition.BOTTOM,
      colorText: Colors.white,
      backgroundColor: Colors.purple[400],
      borderColor: Colors.white,
      borderWidth: 1.0,
      animationDuration: const Duration(milliseconds: 300),
      forwardAnimationCurve: Curves.easeInOut,
      reverseAnimationCurve: Curves.easeInOut,
    );
  }

  void clearListFromPreviousResult() => sourceList.clear();

  void getNewsSource(String category) {
    try {
      isLoading(true);
      Get.find<SourceProvider>().getNewsSource(category).then((NewsCategory newsCategory) {
        sourceList.addAllIf(newsCategory.sources != null, newsCategory.sources!);
        isLoading(false);
        isError(false);
      }).onError((error, stackTrace) {
        if (error is ServerException) {
          callSnackBar('Server Exception', error.message);
        } else {
          callSnackBar('Exception', '$error');
        }
        isError(true);
        isLoading(false);
      });
    } catch (e) {
      callSnackBar('Error', '$e');
      isError(true);
      isLoading(false);
    }
  }
}
