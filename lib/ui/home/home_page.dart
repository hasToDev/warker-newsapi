import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home_controller.dart';
import '../source/source_page.dart';

class HomePage extends StatelessWidget {
  static const String id = '/home';

  HomePage({
    Key? key,
  }) : super(key: key);

  final HomeController c = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(
          'Categories',
          style: GoogleFonts.lexend(
            textStyle: Theme.of(context).textTheme.titleLarge,
            color: Colors.white,
            letterSpacing: 1.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
        ),
      ),
      body: SingleChildScrollView(
        child: GetBuilder<HomeController>(
          init: c,
          builder: (value) {
            return Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    HomeCard(title: value.cardNameList[0], imgPath: value.cardImgList[0]),
                    HomeCard(title: value.cardNameList[2], imgPath: value.cardImgList[2]),
                    HomeCard(title: value.cardNameList[4], imgPath: value.cardImgList[4]),
                    HomeCard(title: value.cardNameList[6], imgPath: value.cardImgList[6]),
                  ],
                ),
                const SizedBox(width: 30.0),
                Column(
                  children: [
                    HomeCard(title: value.cardNameList[1], imgPath: value.cardImgList[1]),
                    HomeCard(title: value.cardNameList[3], imgPath: value.cardImgList[3]),
                    HomeCard(title: value.cardNameList[5], imgPath: value.cardImgList[5]),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

/// BACKUP
// class _HomePageState extends State<HomePage> {
//   HomeController c = Get.find<HomeController>();
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.purple,
//         title: Text(
//           'Categories',
//           style: GoogleFonts.lexend(
//             textStyle: Theme.of(context).textTheme.titleLarge,
//             color: Colors.white,
//             letterSpacing: 1.0,
//             fontWeight: FontWeight.w500,
//           ),
//         ),
//         centerTitle: false,
//       ),
//       body: Center(
//         child: Column(
//           children: [
//             const SizedBox(height: 10.0),
//             ElevatedButton(
//               onPressed: () {
//                 // business entertainment
//                 // general health science
//                 // sports technology
//
//                 // Get.toNamed(CategoryPage.id);
//                 c.getNewsCategory('entertainment');
//               },
//               child: const Text('Go to CategoryPage'),
//             ),
//             const SizedBox(height: 10.0),
//             Obx(() => Text('data length : ${c.categoryList.length}')),
//             const SizedBox(height: 10.0),
//             Obx(() {
//               return c.isLoading.value
//                   ? const SizedBox(
//                       height: 50.0,
//                       width: 50.0,
//                       child: CircularProgressIndicator(
//                         color: Colors.purple,
//                         strokeWidth: 3.4,
//                       ),
//                     )
//                   : const Text('stand by');
//             }),
//             const SizedBox(height: 10.0),
//             GetX<HomeController>(
//               init: c,
//               builder: (value) {
//                 return ListView.builder(
//                   padding: const EdgeInsets.only(top: 6.0, bottom: 20.0),
//                   shrinkWrap: true,
//                   controller: c.scrollController,
//                   itemCount: c.categoryList.length,
//                   itemBuilder: (context, index) {
//                     return Text('${c.categoryList[index].name}');
//                   },
//                 );
//               },
//             ),
//             const SizedBox(height: 10.0),
//             const HomeCard(title: 'Business', imgPath: 'assets/business.jpg'),
//             const SizedBox(height: 10.0),
//             const ArticleCard(
//               urlImg: 'https://avatars.githubusercontent.com/u/35729606?v=4',
//               title:
//                   'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed accumsan molestie sagittis. Pellentesque nec viverra ligula, placerat viverra tellus. Praesent mollis tristique augue, eu egestas arcu hendrerit in',
//               desc:
//                   'Proin quis purus vel purus aliquet varius. Quisque auctor ipsum quis magna venenatis, ut maximus orci pellentesque. Duis et est nisi. Mauris auctor aliquet semper. Donec neque odio, congue nec metus sit amet, consectetur pulvinar felis. Aliquam sagittis ut sem vitae tristique. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
//             ),
//             const SizedBox(height: 10.0),
//             const SourceCard(
//               source:
//                   'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed accumsan molestie sagittis. Pellentesque nec viverra ligula, placerat viverra tellus. Praesent mollis tristique augue, eu egestas arcu hendrerit in',
//               desc:
//                   'Proin quis purus vel purus aliquet varius. Quisque auctor ipsum quis magna venenatis, ut maximus orci pellentesque. Duis et est nisi. Mauris auctor aliquet semper. Donec neque odio, congue nec metus sit amet, consectetur pulvinar felis. Aliquam sagittis ut sem vitae tristique. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',
//             ),
//             const SizedBox(height: 10.0),
//           ],
//         ),
//       ),
//     );
//   }
// }

/// HOME CARD --------------------------------------------
class HomeCard extends StatelessWidget {
  final String title;
  final String imgPath;

  const HomeCard({
    Key? key,
    required this.title,
    required this.imgPath,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(SourcePage.id, arguments: title);
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: Card(
          elevation: 5.0,
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          child: Stack(
            children: [
              Image.asset(
                imgPath,
                height: 115.0,
                width: 160,
                fit: BoxFit.cover,
                gaplessPlayback: true,
                alignment: Alignment.topCenter,
                filterQuality: FilterQuality.high,
              ),
              Container(
                height: 115.0,
                width: 160,
                color: Colors.white54,
                child: const SizedBox(),
              ),
              Positioned(
                top: 12.0,
                left: 12.0,
                child: Text(
                  title,
                  style: Theme.of(context).textTheme.titleLarge?.copyWith(
                        color: Colors.grey[900],
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
