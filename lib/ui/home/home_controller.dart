import 'package:get/get.dart';

class HomeController extends GetxController {
  List<String> cardNameList = <String>[
    'Business',
    'Entertainment',
    'General',
    'Health',
    'Science',
    'Sports',
    'Technology',
  ];

  List<String> cardImgList = <String>[
    'assets/business.jpg',
    'assets/entertainment.jpg',
    'assets/general.jpeg',
    'assets/health.jpg',
    'assets/science.jpg',
    'assets/sport.jpg',
    'assets/technology.jpg',
  ];
}
