import 'package:get/get.dart';
import 'home/home_controller.dart';
import 'source/source_provider.dart';
import 'source/source_controller.dart';
import 'article/article_provider.dart';
import 'article/article_controller.dart';

class UiBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(HomeController(), permanent: true);
    Get.put(SourceProvider(), permanent: true);
    Get.put(SourceController(), permanent: true);
    Get.put(ArticleProvider(), permanent: true);
    Get.put(ArticleController(), permanent: true);
  }
}
