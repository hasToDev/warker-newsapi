import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';
import 'package:warker_newsapi/core/core.dart';
import 'article_controller.dart';
import '../webview/webview_page.dart';

class ArticlePage extends StatelessWidget {
  static const String id = '/article';

  ArticlePage({
    Key? key,
  }) : super(key: key);

  final ArticleController c = Get.find<ArticleController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Get.arguments['name'],
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: GoogleFonts.lexend(
            textStyle: Theme.of(context).textTheme.titleLarge,
            color: Colors.white,
            letterSpacing: 1.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.purple,
        leadingWidth: 56.0,
        leading: IconButton(
          splashColor: Colors.purple[400],
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () async {
            await Future.delayed(const Duration(milliseconds: 125));
            Get.back();
          },
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
        ),
      ),
      body: GetX<ArticleController>(
        init: c,
        initState: (_) {
          c.clearListFromPreviousResult();
          c.getNewsSource(Get.arguments['id']);
        },
        builder: (value) {
          // loading
          if (value.isLoading.isTrue) {
            return const Center(
              child: SizedBox(
                height: 90.0,
                width: 90.0,
                child: CircularProgressIndicator(
                  color: Colors.purple,
                  strokeWidth: 3.0,
                ),
              ),
            );
          }

          // empty list
          if (value.articleList.isEmpty && value.isError.isFalse) {
            return Center(
              child: Text(
                'No Data',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: Colors.purple,
                      fontWeight: FontWeight.w600,
                    ),
              ),
            );
          }

          // try button after error happened
          if (value.articleList.isEmpty && value.isError.isTrue) {
            return Center(
              child: ElevatedButton(
                onPressed: () {
                  c.clearListFromPreviousResult();
                  c.getNewsSource(Get.arguments['id']);
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    'try again',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.8,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ),
              ),
            );
          }

          // content
          return ListView.builder(
            padding: const EdgeInsets.only(top: 6.0, bottom: 20.0),
            shrinkWrap: true,
            controller: c.scrollController,
            itemCount: c.articleList.length,
            itemBuilder: (context, index) {
              return ArticleCard(data: c.articleList[index]);
            },
          );
        },
      ),
    );
  }
}

/// ARTICLE CARD --------------------------------------------
class ArticleCard extends StatelessWidget {
  final ArticleData data;

  const ArticleCard({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(WebviewPage.id, arguments: data.url);
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4.0),
        child: Card(
          elevation: 1.0,
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 90.0,
                  width: 90.0,
                  margin: const EdgeInsets.only(top: 6.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(0.0),
                    child: Image.network(
                      data.urlToImage ?? '',
                      fit: BoxFit.cover,
                      isAntiAlias: true,
                      gaplessPlayback: true,
                      filterQuality: FilterQuality.high,
                      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
                        if (frame != null) return child;
                        return const LoadingImage();
                      },
                      loadingBuilder: (context, child, loadingProgress) {
                        if (loadingProgress == null) return child;
                        return const LoadingImage();
                      },
                      errorBuilder: (context, exception, stacktrace) {
                        return SizedBox(
                          height: 70.0,
                          width: 70.0,
                          child: SvgPicture.asset(
                            'assets/person.svg',
                            color: Colors.grey[900],
                            clipBehavior: Clip.antiAlias,
                            alignment: Alignment.center,
                            fit: BoxFit.fitWidth,
                          ),
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(width: 12.0),
                Flexible(
                  child: Column(
                    children: [
                      Text(
                        data.title ?? 'No Title',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        data.description ?? 'No Description',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(
                              color: Colors.grey[900],
                              fontWeight: FontWeight.normal,
                            ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LoadingImage extends StatelessWidget {
  const LoadingImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80.0,
      width: 80.0,
      child: Shimmer.fromColors(
        baseColor: Colors.grey[200]!,
        highlightColor: Colors.grey[50]!,
        period: const Duration(milliseconds: 1200),
        child: Card(
          elevation: 0.0,
          margin: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: const BorderSide(
              color: Colors.transparent,
              width: 0.0,
            ),
          ),
          child: const SizedBox(),
        ),
      ),
    );
  }
}
