import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewPage extends StatefulWidget {
  static const String id = '/webview';

  const WebviewPage({Key? key}) : super(key: key);

  @override
  State<WebviewPage> createState() => _WebviewPageState();
}

class _WebviewPageState extends State<WebviewPage> {
  late WebViewController controller;
  int loadingPercentage = 0;
  bool isPageError = false;

  @override
  void initState() {
    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(Colors.white)
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            loadingPercentage = progress;
            setState(() {});
          },
          onPageStarted: (String url) {
            loadingPercentage = 0;
            setState(() {});
          },
          onPageFinished: (String url) {
            loadingPercentage = 100;
            setState(() {});
          },
          onWebResourceError: (WebResourceError error) {
            isPageError = true;
            setState(() {});
          },
        ),
      )
      ..loadRequest(Uri.parse(Get.arguments));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        backgroundColor: Colors.purple,
        toolbarHeight: 0.0,
        elevation: 0.0,
        systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
        ),
      ),
      body: Stack(
        children: [
          // content
          WebViewWidget(controller: controller),

          // loading
          if (loadingPercentage < 100)
            IgnorePointer(
              child: LinearProgressIndicator(
                color: Colors.purple,
                value: loadingPercentage / 100.0,
              ),
            ),

          // go back button after page error
          if (isPageError)
            Center(
              child: ElevatedButton(
                onPressed: () {
                  Get.back();
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.purple),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    'go back',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.8,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
