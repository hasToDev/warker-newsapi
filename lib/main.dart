import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'ui/ui_binding.dart';
import 'ui/home/home_page.dart';
import 'ui/source/source_page.dart';
import 'ui/article/article_page.dart';
import 'ui/webview/webview_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.deepPurple[100]),
        splashFactory: InkRipple.splashFactory,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: GoogleFonts.plusJakartaSansTextTheme(Theme.of(context).textTheme),
      ),
      initialBinding: UiBindings(),
      initialRoute: HomePage.id,
      getPages: [
        GetPage(name: HomePage.id, page: () => HomePage()),
        GetPage(name: SourcePage.id, page: () => SourcePage()),
        GetPage(name: ArticlePage.id, page: () => ArticlePage()),
        GetPage(name: WebviewPage.id, page: () => const WebviewPage()),
      ],
    );
  }
}
