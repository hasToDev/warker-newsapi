import 'package:equatable/equatable.dart';
import 'category_data.dart';

class NewsCategory extends Equatable {
  final String? status;
  final List<CategoryData>? sources;

  const NewsCategory({
    required this.status,
    required this.sources,
  });

  factory NewsCategory.fromJson(Map<String, dynamic> json) {
    return NewsCategory(
      status: json['status'],
      sources: json['sources'] != null
          ? List<CategoryData>.from(json['sources'].map((i) => CategoryData.fromJson(i)))
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'sources': sources != null ? sources!.map((v) => v.toJson()).toList() : null,
    };
  }

  @override
  List<Object?> get props => [status, sources];
}
