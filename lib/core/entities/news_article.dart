import 'package:equatable/equatable.dart';
import 'article_data.dart';

class NewsArticle extends Equatable {
  final String? status;
  final int? totalResults;
  final List<ArticleData>? articles;

  const NewsArticle({
    required this.status,
    required this.totalResults,
    required this.articles,
  });

  factory NewsArticle.fromJson(Map<String, dynamic> json) {
    return NewsArticle(
      status: json['status'],
      totalResults: json['totalResults'],
      articles: json['articles'] != null
          ? List<ArticleData>.from(json['articles'].map((i) => ArticleData.fromJson(i)))
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'totalResults': totalResults,
      'sources': articles != null ? articles!.map((v) => v.toJson()).toList() : null,
    };
  }

  @override
  List<Object?> get props => [status, totalResults, articles];
}
